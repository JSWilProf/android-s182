package br.senai.sp.informatica.albunsmusicais.model;

import com.annimon.stream.Collectors;
import com.annimon.stream.Optional;
import com.annimon.stream.Stream;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.function.ToDoubleBiFunction;

//Singleton
public class AlbumDao {
    public static AlbumDao dao = new AlbumDao();
    private static List<Album> lista = new ArrayList<>();
    private static long id = 0;

    static {
        lista.add(new Album(id++, "AC/DC", "Rock or Burst", "Rock",
                new GregorianCalendar(2014, 10, 1).getTime(),
                null, false));
    }

    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private AlbumDao() {}

    public void addListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }

    public Album localizar(long id) {
        Album obj = Stream.of(lista)
                .filter(album -> album.getId() == id)
                .findFirst()
                .orElse(null);

        return obj != null ? obj.clone() : null;
    }

    private Album localizarPorAlbum(String nome) {
        /* Pegue a lista de Albuns,
           filtre por nome, onde o nome do Album seja igual ao informado
           separe o primeiro que encontrar
           caso não encontre retorne nulo
        */

        return Stream.of(lista)
                .filter(album -> album.getAlbum().equals(nome))
                .findFirst()
                .orElse(null);

//        Album obj = null;
//
//        for (Album album : lista) {
//            if (album.getAlbum().equals(nome)) {
//                obj = album;
//                break;
//            }
//        }
//        return obj;
    }

    public void salvar(Album obj) {
        if(obj.getId() != null) {
            Album novo = localizar(obj.getId());

            if(novo != null) {
                Album atual = novo.clone();
                novo.update(obj);
                lista.set(lista.indexOf(atual), novo);
                changeSupport.firePropertyChange("albumModificado" , atual, novo);
            }
        }
    }

    public void inserir(Album obj) {
        if(obj.getId() == null && localizarPorAlbum(obj.getAlbum()) == null) {
            obj.setId(id++);
            lista.add(obj);
            changeSupport.firePropertyChange("albumNovo", null, obj);
        }
    }

    private void remover(Long id) {
        Album album = localizar(id);
        lista.remove(album);
        changeSupport.firePropertyChange("delAlbum", album, null);
    }

    public void removerMarcados() {
        Stream.of(
            // Separe em uma lista todos os IDs dos Albuns marcados para apagar
            Stream.of(lista)
                .filter(album -> album.isDel())
                .map(album -> album.getId())
                .collect(Collectors.toList())
        ).forEach(id -> remover(id));
    }

    public long existeAlbunsADeletar() {
        return Stream.of(lista)
                .filter(album -> album.isDel())
                .count();
    }

    public void limparMarcados() {
        Stream.of(lista)
                .filter(album -> album.isDel())
                .map(album -> {
                    Album novo = album.clone();
                    novo.setDel(false);
                    return novo;
                }).forEach(novo -> salvar(novo));

//        for (Album album: lista) {
//            if(album.isDel()) {
//                Album novo = album.clone();
//                novo.setDel(false);
//                salvar(novo);
//            }
//        }
    }

    public List<Album> getAlbuns() {
        return Stream.of(lista)
                .map(album -> album.clone())
                .collect(Collectors.toList());
    }
}
