package br.senai.sp.informatica.albunsmusicais.model;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

public class AlbumViewModel extends ViewModel implements PropertyChangeListener {
    private AlbumDao dao = AlbumDao.dao;
    public MutableLiveData< List<Album> > data;

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        data.postValue(dao.getAlbuns());
    }

    public LiveData<List<Album>> getAlbuns() {
        if(data == null) {
            data = new MutableLiveData<>();
            data.postValue(dao.getAlbuns());
            dao.addListener(this);
        }
        return data;
    }

    public void inserir(Album album) {
        dao.inserir(album);
    }

    public void salvar(Album album) {
        dao.salvar(album);
    }

    public Album localizar(long id) {
        return dao.localizar(id);
    }

    public void removerMarcados() {
        dao.removerMarcados();
    }

    public boolean existeAlbunsADeletar() {
        return dao.existeAlbunsADeletar() > 0;
    }

    public void limparMarcados() {
        dao.limparMarcados();
    }
}
