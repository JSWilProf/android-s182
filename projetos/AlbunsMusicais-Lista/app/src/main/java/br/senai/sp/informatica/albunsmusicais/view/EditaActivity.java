package br.senai.sp.informatica.albunsmusicais.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.app.ActionBar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import br.senai.sp.informatica.albunsmusicais.R;
import br.senai.sp.informatica.albunsmusicais.lib.Utilitarios;
import br.senai.sp.informatica.albunsmusicais.model.Album;
import br.senai.sp.informatica.albunsmusicais.model.AlbumViewModel;

@SuppressLint("SimpleDateFormat")
public class EditaActivity extends AppCompatActivity {
    private TextInputEditText edBanda;
    private TextInputEditText edAlbum;
    private TextInputEditText edGenero;
    private TextInputEditText edLancamento;

    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd 'de' MMM 'de' yyyy");
    private Calendar calendar = Calendar.getInstance();

    private AlbumViewModel viewModel;
    private Album album;

    private ImageView ivCapa;
    private Uri imageUrl;

    private static final int REQUEST_IMAGE_GALERY = 0;
    private static final int REQUEST_GALERY_PERMISSION = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edita);

        edBanda = findViewById(R.id.edBanda);
        edAlbum = findViewById(R.id.edAlbum);
        edGenero = findViewById(R.id.edGenero);
        edLancamento = findViewById(R.id.edLancamento);

        ivCapa = findViewById(R.id.ivCapa);

        viewModel = ViewModelProviders.of(this).get(AlbumViewModel.class);

        Bundle extra = getIntent().getExtras();
        if(extra != null) {
            long id = extra.getLong("id");
            album = viewModel.localizar(id);
            if(album != null) {
                edBanda.setText(album.getBanda());
                edAlbum.setText(album.getAlbum());
                edGenero.setText(album.getGenero());
                edLancamento.setText(album.getDataDeLancamento());

                calendar.setTime(album.getLancamento());

                byte[] capaBase64 = album.getCapa();
                if(capaBase64 != null) {
                    Bitmap bitmap = Utilitarios.bitmapFromBase64(capaBase64);
                    ivCapa.setImageBitmap(bitmap);
                }
            }
        }

        ActionBar bar = getActionBar();
        if(bar != null) {
            bar.setHomeButtonEnabled(true);
            bar.setDisplayHomeAsUpEnabled(true);
        }

        Utilitarios.hideKeyboard(this);

    }

    public void okDateClicked(View view) {
        DateDialog.makeDialog(calendar, edLancamento).show(getFragmentManager(), "");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_salvar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == android.R.id.home) {
            setResult(RESULT_CANCELED);
        } else {
            boolean editar = true;

            if(album == null) {
                album = new Album();
                editar = false;
            }

            album.setBanda(edBanda.getText().toString());
            album.setAlbum(edAlbum.getText().toString());
            album.setGenero(edGenero.getText().toString());
            album.setLancamento(calendar.getTime());

            Bitmap bitmap = Utilitarios.bitmapFromImageView(ivCapa);
            if(bitmap != null) {
                album.setCapa(Utilitarios.bitmapToBase64(bitmap));
            } else {
                album.setCapa(null);
            }

            if(editar) {
                viewModel.salvar(album);
            } else {
                viewModel.inserir(album);
            }

            setResult(RESULT_OK);
        }
        finish();

        return true;
    }

    public void onCapaClicked(View view) {
        abrirGaleria();
    }

    public void abrirGaleria() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        if(intent.resolveActivity(getPackageManager()) != null) {
            if(ContextCompat.checkSelfPermission(getBaseContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE) !=
                            PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[] {
                        Manifest.permission.READ_EXTERNAL_STORAGE },
                        REQUEST_GALERY_PERMISSION);
            } else {
                startActivityForResult(Intent.createChooser(intent,
                        "Selecione a Foto"), REQUEST_IMAGE_GALERY);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean autorizado = true;

        for(int resultado : grantResults) {
            if(resultado == PackageManager.PERMISSION_DENIED) {
                autorizado = false;
                break;
            }
        }

        switch (requestCode) {
            case REQUEST_GALERY_PERMISSION:
                if(autorizado) {
                    abrirGaleria();
                } else {
                    Toast.makeText(this, "O Acesso à Galeria de Fotos foi negado!", Toast.LENGTH_LONG).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_IMAGE_GALERY) {
            if(data != null) {
                try {
                    imageUrl = data.getData();

                    Bitmap bitmap = Utilitarios.setPic(ivCapa.getWidth(), ivCapa.getHeight(), imageUrl, this);
                    ivCapa.setImageBitmap(bitmap);
                    ivCapa.invalidate();
                } catch (IOException ex) {
                    Toast.makeText(this, "Falha ao abrir a Foto", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if(imageUrl != null) {
            outState.putParcelable("uri", imageUrl);
        }

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        imageUrl = savedInstanceState.getParcelable("uri");
        carregarFoto(imageUrl);
    }

    private void carregarFoto(Uri imageUrl) {
        try {
            if(imageUrl != null) {
                Bitmap bitmap = Utilitarios.setPic(ivCapa.getWidth(), ivCapa.getHeight(), imageUrl, this);
               ivCapa.setImageBitmap(bitmap);
               ivCapa.invalidate();
            } else {
                if(album != null) {
                    byte[] capaBase64 = album.getCapa();
                    if(capaBase64 != null) {
                        Bitmap bitmap = Utilitarios.bitmapFromBase64(capaBase64);
                        ivCapa.setImageBitmap(bitmap);
                        ivCapa.invalidate();
                    }
                }
            }
        } catch (IOException ex) {
            Toast.makeText(this, "Falha ao abrir a Foto", Toast.LENGTH_LONG).show();
        }
    }
}
