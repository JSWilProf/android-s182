package br.senai.sp.informatica.albunsmusicais.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import br.senai.sp.informatica.albunsmusicais.R;
import br.senai.sp.informatica.albunsmusicais.model.AlbumViewModel;

public class ListaActivity extends AppCompatActivity implements OnAlbumClicked {
    private AlbumViewModel viewModel;
    private AlbumAdapter adapter;

    private MenuItem itemEditar;
    private MenuItem itemApagar;

    private boolean editar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        // Inicializa O Adapter e o RecyclerView
        adapter = new AlbumAdapter(this, this);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        // Inicializa o ViewModel
        viewModel = ViewModelProviders.of(this).get(AlbumViewModel.class);
        viewModel.getAlbuns().observe(this, albuns -> adapter.submitList(albuns));

        FloatingActionButton btAdicionar = findViewById(R.id.btAdicionar);
        btAdicionar.setOnClickListener(view -> editarAlbum(null));
    }

    public void editarAlbum(Long id) {
        Intent intent = new Intent(this, EditaActivity.class);
        if(id != null) intent.putExtra("id", id);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editar, menu);
        itemEditar = menu.findItem(R.id.item_editar);
        itemApagar = menu.findItem(R.id.item_apagar);

        setEditar(false);

        return true;
    }

    private void setEditar(boolean valor) {
        editar = valor;
        adapter.setEditar(valor);
        itemEditar.setVisible(!valor);
        itemApagar.setVisible(valor);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.item_editar) {
            setEditar(true);
        } else {
            if(viewModel.existeAlbunsADeletar()) {
                new AlertDialog.Builder(this)
                    .setMessage("Confirma a exclusão deste(s) Amigo(s)")
                    .setPositiveButton("Sim", (x, y) -> viewModel.removerMarcados())
                    .setNegativeButton("Não", (x, y) -> viewModel.limparMarcados())
                    .create()
                    .show();
            }
            setEditar(false);
        }

        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("editar", editar);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        editar = savedInstanceState.getBoolean("editar");
    }
}
