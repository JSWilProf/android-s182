package br.senai.sp.informatica.albunsmusicais.lib;

public interface DatabaseAction<Tipo> {
    void doAction(Tipo obj);
}
