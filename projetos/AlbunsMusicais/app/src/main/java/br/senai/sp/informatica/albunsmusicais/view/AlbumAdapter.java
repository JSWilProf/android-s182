package br.senai.sp.informatica.albunsmusicais.view;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import br.senai.sp.informatica.albunsmusicais.R;
import br.senai.sp.informatica.albunsmusicais.lib.DatabaseException;
import br.senai.sp.informatica.albunsmusicais.lib.Utilitarios;
import br.senai.sp.informatica.albunsmusicais.model.Album;
import br.senai.sp.informatica.albunsmusicais.model.AlbumViewModel;

public class AlbumAdapter extends ListAdapter<Album, AlbumAdapter.AlbumViewHolder> {
    private AlbumViewModel viewModel;
    private boolean editar;
    private OnAlbumClicked executor;
    private Activity activity;

    public AlbumAdapter(ListaActivity activity, OnAlbumClicked executor) {
        super(new DiffUtil.ItemCallback<Album>() {
            @Override
            public boolean areItemsTheSame(Album oldItem, Album newItem) {
                return oldItem.getId() == newItem.getId();
            }

            @Override
            public boolean areContentsTheSame(Album oldItem, Album newItem) {
                return oldItem.equals(newItem);
            }
        });

        this.activity = activity;
        viewModel = ViewModelProviders.of(activity).get(AlbumViewModel.class);
        this.executor = executor;
    }

    public void setEditar(boolean valor) {
        this.editar = valor; //editar;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public AlbumAdapter.AlbumViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater svc = LayoutInflater.from(parent.getContext());
        View layout = svc.inflate(R.layout.item_album, parent, false);
        return new AlbumViewHolder(layout);
    }

    @Override
    public void onBindViewHolder(@NonNull AlbumAdapter.AlbumViewHolder holder, int position) {
        Album album = getItem(position);
        holder.bindTo(album);
    }

    public class AlbumViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivCapa;
        private TextView tvBanda;
        private TextView tvAlbum;
        private TextView tvGenero;
        private TextView tvLancamento;
        private CheckBox checkBox;
        private View view;

        public AlbumViewHolder(View view) {
            super(view);

            this.view = view;
            ivCapa = view.findViewById(R.id.ivCapa);
            tvBanda = view.findViewById(R.id.tvBanda);
            tvAlbum = view.findViewById(R.id.tvAlbum);
            tvGenero = view.findViewById(R.id.tvGenero);
            tvLancamento = view.findViewById(R.id.tvLancamento);
            checkBox = view.findViewById(R.id.checkBox);
        }

        public void bindTo(Album album) {
            tvBanda.setText(album.getBanda());
            tvAlbum.setText(album.getAlbum());
            tvGenero.setText(album.getGenero());
            tvLancamento.setText(album.getDataDeLancamento());

            carregaFoto(ivCapa, album.getCapa(), album.getBanda());

            checkBox.setChecked(album.isDel());
            checkBox.setTag(album.getId());
            checkBox.setVisibility(editar ? View.VISIBLE : View.GONE);
            checkBox.setOnClickListener(view -> {
                try {
                    Album obj = viewModel.localizar((Long)view.getTag());
                    obj.setDel(!obj.isDel());
                    viewModel.salvar(obj);
                } catch(DatabaseException ex) {
                    Toast.makeText(activity, ex.getMessage(), Toast.LENGTH_LONG).show();
                }
            });

            view.setTag(album.getId());
            view.setOnClickListener(view -> executor.editarAlbum((Long)view.getTag()));
        }

        private void carregaFoto(ImageView capa, byte[] foto, String nomeBanda) {
            if(foto != null) {
                Bitmap bitmap = Utilitarios.bitmapFromBase64(foto);
                capa.setImageBitmap(bitmap);
            } else {
                String letra = nomeBanda.substring(0, 1).toUpperCase();
                Bitmap bitmap = Utilitarios.circularBitmapAndText(
                        Color.parseColor("#2C3CCC"), 200, 200, letra);
                capa.setBackgroundColor(Color.TRANSPARENT);
                capa.setImageBitmap(bitmap);
            }
        }

    }
}












