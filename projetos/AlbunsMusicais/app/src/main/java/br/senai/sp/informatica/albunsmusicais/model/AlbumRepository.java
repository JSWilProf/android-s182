package br.senai.sp.informatica.albunsmusicais.model;

import android.arch.lifecycle.LiveData;
import android.arch.paging.DataSource;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;

import java.util.concurrent.ExecutionException;

import br.senai.sp.informatica.albunsmusicais.Main;
import br.senai.sp.informatica.albunsmusicais.R;
import br.senai.sp.informatica.albunsmusicais.lib.Action;
import br.senai.sp.informatica.albunsmusicais.lib.DatabaseAction;
import br.senai.sp.informatica.albunsmusicais.lib.DatabaseException;
import br.senai.sp.informatica.albunsmusicais.lib.Query;

public class AlbumRepository {
    private AlbumDao dao;
    private LiveData<PagedList<Album>> albuns;

    private String ordenacao;

    public AlbumRepository() {
        AlbumDatabase db = AlbumDatabase.getInstance();
        dao = db.albumDao();
        ordenacao = getOrdenacao();
        //DataSource.Factory<Integer, Album> factory = dao.getAlbuns();
        albuns = new LivePagedListBuilder<Integer, Album>(getFactory(), 10).build();
    }

    private DataSource.Factory<Integer, Album> getFactory() {
        switch (ordenacao) {
            case "Album":
                return dao.getAlbunsPorAlbum();
            case "Banda":
                return dao.getAlbunsPorBanda();
            default:
                return dao.getAlbunsPorLancamento();
        }
    }

    private String getOrdenacao() {
        Context context = Main.getContext();
        Resources resources = context.getResources();
        String ordemKey = resources.getString(R.string.ordem_key);
        String ordemDefault = resources.getString(R.string.ordem_default);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(ordemKey, ordemDefault);
    }

    public Album localizar(long id) throws DatabaseException {
       try {
           return new Query<Long, Album>(key -> dao.localizar(key)).execute(id).get();
       } catch (ExecutionException | InterruptedException ex) {
           throw new DatabaseException("Falha na execução da consulta aon ID: " + id);
       }
    }

    public void salvar(Album album) {
        new Action<Album>(obj -> dao.salvar(obj)).execute(album);
    }

    public void inserir(Album album) {
        new Action<Album>(obj -> dao.inserir(obj)).execute(album);
    }

    public void removerMarcados() {
        new Action<Void>(x -> dao.removerMarcados()).execute();
    }

    public int existeAlbunsADeletar() throws DatabaseException {
        try {
            return new Query<Void, Integer>(x -> dao.existeAlbunsADeletar()).execute().get();
        } catch (ExecutionException | InterruptedException ex) {
            throw new DatabaseException("Falha em determinar se existem Albuns à remover");
        }
    }

    public void limparMarcados() {
        new Action<Void>(x -> dao.limparMarcados()).execute();
    }

    public LiveData<PagedList<Album>> getAlbuns() {
        String ordenacaoAtual = getOrdenacao();
        if(!ordenacao.equals(ordenacaoAtual)) {
            ordenacao = ordenacaoAtual;
            albuns = new LivePagedListBuilder<Integer, Album>(getFactory(), 10).build();
        }
        return albuns;
    }
}
