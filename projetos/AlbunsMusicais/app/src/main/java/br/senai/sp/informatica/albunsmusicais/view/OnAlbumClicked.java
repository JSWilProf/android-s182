package br.senai.sp.informatica.albunsmusicais.view;

public interface OnAlbumClicked {
    void editarAlbum(Long id);
}
