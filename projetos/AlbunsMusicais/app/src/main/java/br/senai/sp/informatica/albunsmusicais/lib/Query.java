package br.senai.sp.informatica.albunsmusicais.lib;

import android.os.AsyncTask;

public class Query<Tipo, Retorno> extends AsyncTask<Tipo, Void, Retorno> {
    private DatabaseQuery<Tipo, Retorno> query;

    public Query(DatabaseQuery<Tipo, Retorno> query) {
        this.query = query;
    }

    @Override
    protected Retorno doInBackground(Tipo... obj) {
        if(obj instanceof Void[]) {
            return query.doAction(null);
        } else {
            return query.doAction(obj[0]);
        }
    }
}
