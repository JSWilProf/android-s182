package br.senai.sp.informatica.albunsmusicais.lib;

// Copiar a Classe PhotoEditorException
public class PhotoEditorException extends Exception {
    public PhotoEditorException(String detailMessage) {
        super(detailMessage);
    }
}
