package br.senai.sp.informatica.albunsmusicais.view;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

import br.senai.sp.informatica.albunsmusicais.R;
import br.senai.sp.informatica.albunsmusicais.lib.PhotoActions;
import br.senai.sp.informatica.albunsmusicais.lib.Utilitarios;

public class UserActivity extends AppCompatActivity
        implements PopupMenu.OnMenuItemClickListener {
    public static final String NOME_USUARIO = "NOME";
    public static final String EMAIL_USUARIO = "EMAIL";
    public static final String FOTO_USUARIO = "FOTO";

    private TextInputEditText edNome;
    private TextInputEditText edEmail;

    private ImageView ivFoto;
    private File fotoUrl;
    private boolean novaFoto;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        edNome = findViewById(R.id.edNome);
        edEmail = findViewById(R.id.edEmail);
        ivFoto = findViewById(R.id.ivFoto);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        edNome.setText(preferences.getString(NOME_USUARIO, ""));
        edEmail.setText(preferences.getString(EMAIL_USUARIO, ""));

        String fotoString = preferences.getString(FOTO_USUARIO, null);
        if(fotoString != null) {
            Bitmap bitmap = Utilitarios.bitmapFromBase64(fotoString.getBytes());
            ivFoto.setImageBitmap(bitmap);
            novaFoto = false;
        }

        Utilitarios.hideKeyboard(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_salvar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.item_salvar) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = preferences.edit();

            editor.putString(NOME_USUARIO, edNome.getText().toString());
            editor.putString(EMAIL_USUARIO, edEmail.getText().toString());

            Bitmap bitmap = Utilitarios.bitmapFromImageView(ivFoto);
            if(bitmap != null) {
                editor.putString(FOTO_USUARIO, new String(Utilitarios.bitmapToBase64(bitmap)));
            } else {
                editor.putString(FOTO_USUARIO, null);
            }

            editor.apply();
        }

        finish();
        return true;
    }

    public void onFotoClicked(View view) {
        PopupMenu popupMenu = new PopupMenu(this, ivFoto);
        popupMenu.getMenuInflater().inflate(R.menu.menu_foto, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(this);
        popupMenu.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        fotoUrl = new PhotoActions(this).fotoMenu(item, ivFoto,
                R.id.ac_tirar_foto, R.id.ac_galeria_foto, R.id.ac_excluir_foto);
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean autorizado = true;

        for(int resultado : grantResults) {
            if(resultado == PackageManager.PERMISSION_DENIED) {
                autorizado = false;
                break;
            }
        }

        switch (requestCode) {
            case PhotoActions.REQUEST_CAMERA_PERMISSION:
                try {
                   if(autorizado) {
                       fotoUrl = new PhotoActions(this).startCamera();
                   } else {
                       Toast.makeText(this, "O Acesso à Câmera foi negado!", Toast.LENGTH_LONG).show();
                   }
                } catch (IOException ex) {
                    Toast.makeText(this, "Houve problemas em Salvar a Foto", Toast.LENGTH_LONG).show();
                }
                break;
            case PhotoActions.REQUEST_GALERY_PERMISSION:
                if(autorizado) {
                    new PhotoActions(this).openGalery();
                } else {
                    Toast.makeText(this, "O Acesso à Galeria de Fotos foi negado!", Toast.LENGTH_LONG).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        novaFoto = new PhotoActions(this).fotoResult(requestCode, resultCode, data, ivFoto, fotoUrl, false);
    }
}
