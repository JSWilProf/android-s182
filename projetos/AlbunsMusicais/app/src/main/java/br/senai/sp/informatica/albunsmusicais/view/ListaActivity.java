package br.senai.sp.informatica.albunsmusicais.view;

import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import br.senai.sp.informatica.albunsmusicais.R;
import br.senai.sp.informatica.albunsmusicais.lib.DatabaseException;
import br.senai.sp.informatica.albunsmusicais.lib.Utilitarios;
import br.senai.sp.informatica.albunsmusicais.model.AlbumViewModel;

public class ListaActivity extends AppCompatActivity
        implements OnAlbumClicked, NavigationView.OnNavigationItemSelectedListener {
    private AlbumViewModel viewModel;
    private AlbumAdapter adapter;

    private MenuItem itemEditar;
    private MenuItem itemApagar;

    private boolean editar;

    private DrawerLayout drawer;

    private ImageView ivFoto;
    private TextView edNome;
    private TextView edEmail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Inicializa O Adapter e o RecyclerView
        adapter = new AlbumAdapter(this, this);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        // Inicializa o ViewModel
        viewModel = ViewModelProviders.of(this).get(AlbumViewModel.class);
        viewModel.getAlbuns().observe(this, albuns -> adapter.submitList(albuns));

        FloatingActionButton btAdicionar = findViewById(R.id.btAdicionar);
        btAdicionar.setOnClickListener(view -> editarAlbum(null));

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View cabecalho = navigationView.getHeaderView(0);
        ivFoto = cabecalho.findViewById(R.id.ivFoto);
        edNome = cabecalho.findViewById(R.id.tvNome);
        edEmail = cabecalho.findViewById(R.id.tvEmail);
    }

    @Override
    protected void onStart() {
        super.onStart();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        edNome.setText(preferences.getString(UserActivity.NOME_USUARIO, ""));
        edEmail.setText(preferences.getString(UserActivity.EMAIL_USUARIO, ""));

        String fotoString = preferences.getString(UserActivity.FOTO_USUARIO, null);
        if(fotoString != null) {
            Bitmap bitmap = Utilitarios.bitmapFromBase64(fotoString.getBytes());
            ivFoto.setImageBitmap(Utilitarios.toCircularBitmap(bitmap));
        } else {
            ivFoto.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_face_black_24dp));
        }
    }

    @Override
    public void onBackPressed() {
        if(drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.nav_pref) {
            Intent intent = new Intent(this, PreferenciasActivity.class);
            startActivityForResult(intent, 0);
        } else if(id == R.id.nav_perfil) {
            Intent intent = new Intent(this, UserActivity.class);
            startActivity(intent);
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        viewModel.getAlbuns().removeObservers(this);

        // Inicializa o ViewModel
        viewModel = ViewModelProviders.of(this).get(AlbumViewModel.class);
        viewModel.getAlbuns().observe(this, albuns -> adapter.submitList(albuns));
    }

    public void editarAlbum(Long id) {
        Intent intent = new Intent(this, EditaActivity.class);
        if(id != null) intent.putExtra("id", id);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editar, menu);
        itemEditar = menu.findItem(R.id.item_editar);
        itemApagar = menu.findItem(R.id.item_apagar);

        setEditar(false);

        return true;
    }

    private void setEditar(boolean valor) {
        editar = valor;
        adapter.setEditar(valor);
        itemEditar.setVisible(!valor);
        itemApagar.setVisible(valor);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.item_editar) {
            setEditar(true);
        } else {
            try {
                if(viewModel.existeAlbunsADeletar()) {
                    new AlertDialog.Builder(this)
                            .setMessage("Confirma a exclusão deste(s) Amigo(s)")
                            .setPositiveButton("Sim", (x, y) -> viewModel.removerMarcados())
                            .setNegativeButton("Não", (x, y) -> viewModel.limparMarcados())
                            .create()
                            .show();
                }
                setEditar(false);
            } catch(DatabaseException ex) {
                Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
            }
        }

        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean("editar", editar);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        editar = savedInstanceState.getBoolean("editar");
    }
}
