package br.senai.sp.informatica.albunsmusicais.model;

import android.arch.paging.DataSource;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

@Dao
public interface AlbumDao {
    @Query("select * from album where id = :id")
    public Album localizar(long id);

    @Update
    public void salvar(Album obj);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void inserir(Album obj);

    @Query("delete from album where del = '1' ")
    public void removerMarcados();

    @Query("select count(*) from album where del = '1' ")
    public int existeAlbunsADeletar();

    @Query("update album set del = '0' where del = '1' ")
    public void limparMarcados();

    @Query("select * from album order by banda")
    public DataSource.Factory<Integer, Album> getAlbuns();
}
