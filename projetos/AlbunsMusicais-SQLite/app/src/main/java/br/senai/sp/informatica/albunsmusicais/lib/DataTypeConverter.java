package br.senai.sp.informatica.albunsmusicais.lib;

import android.arch.persistence.room.TypeConverter;

import java.util.Date;

public class DataTypeConverter {
    @TypeConverter
    public static Date toDate(Long valor) {
        return valor == null ? null : new Date(valor);
    }

    @TypeConverter
    public static Long toLong(Date valor) {
        return valor == null ? null : valor.getTime();
    }
}
