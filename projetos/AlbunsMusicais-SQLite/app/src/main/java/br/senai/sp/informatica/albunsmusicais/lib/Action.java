package br.senai.sp.informatica.albunsmusicais.lib;

import android.os.AsyncTask;

public class Action<Tipo> extends AsyncTask<Tipo, Void, Void> {
    private DatabaseAction<Tipo> action;

    public Action(DatabaseAction<Tipo> action) {
        this.action = action;
    }

    @Override
    protected Void doInBackground(Tipo... obj) {
        if (obj instanceof Void[]) {
            action.doAction(null);
        } else {
            action.doAction(obj[0]);
        }

        return null;
    }
}
