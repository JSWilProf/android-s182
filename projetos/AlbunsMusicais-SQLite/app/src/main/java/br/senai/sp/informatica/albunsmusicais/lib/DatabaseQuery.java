package br.senai.sp.informatica.albunsmusicais.lib;

public interface DatabaseQuery<Tipo, Retorno> {
    Retorno doAction(Tipo obj);
}
