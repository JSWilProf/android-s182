package br.senai.sp.informatica.albunsmusicais.model;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.arch.paging.PagedList;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

import br.senai.sp.informatica.albunsmusicais.lib.DatabaseException;

public class AlbumViewModel extends ViewModel {
    private AlbumRepository dao = new AlbumRepository();
    public LiveData<PagedList<Album>> data = dao.getAlbuns();

    public LiveData<PagedList<Album>> getAlbuns() {
        return data;
    }

    public void inserir(Album album) {
        dao.inserir(album);
    }

    public void salvar(Album album) {
        dao.salvar(album);
    }

    public Album localizar(long id) throws DatabaseException {
        return dao.localizar(id);
    }

    public void removerMarcados() {
        dao.removerMarcados();
    }

    public boolean existeAlbunsADeletar() throws DatabaseException {
        return dao.existeAlbunsADeletar() > 0;
    }

    public void limparMarcados() {
        dao.limparMarcados();
    }
}
