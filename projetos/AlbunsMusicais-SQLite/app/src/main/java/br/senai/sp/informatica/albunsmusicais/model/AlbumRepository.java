package br.senai.sp.informatica.albunsmusicais.model;

import android.arch.lifecycle.LiveData;
import android.arch.paging.DataSource;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;

import java.util.concurrent.ExecutionException;

import br.senai.sp.informatica.albunsmusicais.lib.Action;
import br.senai.sp.informatica.albunsmusicais.lib.DatabaseException;
import br.senai.sp.informatica.albunsmusicais.lib.Query;

public class AlbumRepository {
    private AlbumDao dao;
    private LiveData<PagedList<Album>> albuns;

    public AlbumRepository() {
        AlbumDatabase db = AlbumDatabase.getInstance();
        dao = db.albumDao();
        DataSource.Factory<Integer, Album> factory = dao.getAlbuns();
        albuns = new LivePagedListBuilder<Integer, Album>(factory, 10).build();
    }

    public Album localizar(long id) throws DatabaseException {
       try {
           return new Query<Long, Album>(key -> dao.localizar(key)).execute(id).get();
       } catch (ExecutionException | InterruptedException ex) {
           throw new DatabaseException("Falha na execução da consulta aon ID: " + id);
       }
    }

    public void salvar(Album album) {
        new Action<Album>(obj -> dao.salvar(obj)).execute(album);
    }

    public void inserir(Album album) {
        new Action<Album>(obj -> dao.inserir(obj)).execute(album);
    }

    public void removerMarcados() {
        new Action<Void>(x -> dao.removerMarcados()).execute();
    }

    public int existeAlbunsADeletar() throws DatabaseException {
        try {
            return new Query<Void, Integer>(x -> dao.existeAlbunsADeletar()).execute().get();
        } catch (ExecutionException | InterruptedException ex) {
            throw new DatabaseException("Falha em determinar se existem Albuns à remover");
        }
    }

    public void limparMarcados() {
        new Action<Void>(x -> dao.limparMarcados()).execute();
    }

    public LiveData<PagedList<Album>> getAlbuns() {
        return albuns;
    }
}
