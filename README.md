# Repositório da Turma Android-S1802 - Senai Informatica 1.32
## Bem Vindo
Aqui são disponibilizados os projetos Android, as apresentações, exercícios e respostas.
## Como obter
Para obter uma cópia deste conteúdo basta utilizar o comando:

```
git clone git@gitlab.com:JSWilProf/Android-S1802.git
```

Também é possível fazer o download através do link
[Andorid-S1802](https://gitlab.com/JSWilProf/Android-S1802)

# Ementa

## Módulo Principal (40h)

### Interface de Programação - API
- Construção de aplicações para Android
- Construção de interfaces de usuário
- Navegação de telas
- Diálogos
- Tratamento de eventos
- Operaç∫oes de armazenamento de informacões em Banco de Dados
- Definição de Activity, Intent, View e Adapter

### Design Patterns
- Singleton
- Delegate
- OBservable
- Data Access Object
